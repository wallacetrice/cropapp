package com.example.mcdenny.crops;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mcdenny.crops.common.CommonUsed;
import com.example.mcdenny.crops.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference reference;
    Button logIn, signUp;
    FancyButton signupButton;
    FancyButton signinButton;
    ImageView closeImgIn, closeImgOut;
    Dialog loginDialog, signupDialog;
    RelativeLayout rootLayout;
    CardView loginLayout, signupLayout;
    TextView forgotPsd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginDialog = new Dialog(this);
        signupDialog = new Dialog(this);

        forgotPsd = (TextView) findViewById(R.id.forgotPwd) ;
        logIn = (Button) findViewById(R.id.btn_login);
        signUp = (Button) findViewById(R.id.btn_signup);
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        //init firebase
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("User");

        //forgot password
        forgotPsd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgotIntent = new Intent(MainActivity.this, ForgotPassword.class);
                startActivity(forgotIntent);
            }
        });
        //listening the login button
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoginDialog();
            }
        });

        //listening the sign up button
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSignUpDialog();
            }
        });
    }

    //when login button clicked
    private void showLoginDialog() {

        loginDialog.setContentView(R.layout.login_layout);
        loginDialog.setCancelable(false);
        final MaterialEditText phone = (MaterialEditText) loginDialog.findViewById(R.id.txt_phone);
        final MaterialEditText pswd = (MaterialEditText) loginDialog.findViewById(R.id.txt_password);
        signinButton = (FancyButton) loginDialog.findViewById(R.id.signinButton);
        closeImgIn = (ImageView) loginDialog.findViewById(R.id.closeImgIn);
        loginLayout = (CardView) loginDialog.findViewById(R.id.loginLayout);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(phone.getText().toString())) {
                    phone.setError("You must fill in the phone number!");
                    phone.requestFocus();
                } else if (TextUtils.isEmpty(phone.getText().toString())) {
                    pswd.setError("You must fill in the password!");
                    pswd.requestFocus();
                } else {
                    reference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //checking if the user exists
                            if (dataSnapshot.child(phone.getText().toString()).exists()) {

                                //getting the users information

                                User newUser = dataSnapshot.child(phone.getText().toString()).getValue(User.class);
                                assert newUser != null;
                                newUser.setPhone(phone.getText().toString());
                                if (newUser.getPassword().equals(pswd.getText().toString())) {
                                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                    CommonUsed.user_current = newUser;//the user details are stored in user_current variable
                                    startActivity(intent);
                                    finish();//stops the login activity
                                } else {
                                    Snackbar.make(loginLayout, "Incorrect password!", Snackbar.LENGTH_SHORT).show();
                                }
                            } else {
                                Snackbar.make(loginLayout, "User does'nt exist!", Snackbar.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        closeImgIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginDialog.dismiss();
            }
        });
        loginDialog.show();

    }

    //when sign up button clicked
    private void showSignUpDialog() {
        signupDialog.setContentView(R.layout.signup_layout);
        signupDialog.setCancelable(false);
        final MaterialEditText name = (MaterialEditText) signupDialog.findViewById(R.id.txt_name);
        final MaterialEditText rphone = (MaterialEditText) signupDialog.findViewById(R.id.txt_rPhone);
        final MaterialEditText rpswd = (MaterialEditText) signupDialog.findViewById(R.id.txt_rpassword);
        signupButton = (FancyButton) signupDialog.findViewById(R.id.signupButton);
        closeImgOut = (ImageView) signupDialog.findViewById(R.id.closeImgOut);
        signupLayout = (CardView) signupDialog.findViewById(R.id.signupLayout);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().isEmpty()) {
                    name.setError("You must fill in the name!");
                    name.requestFocus();
                } else if (rphone.getText().toString().isEmpty()) {
                    rphone.setError("You must fill in the phone number!");
                    rphone.requestFocus();
                } else if (rpswd.getText().toString().isEmpty()) {
                    rpswd.setError("You must fill in the password!");
                    rpswd.requestFocus();
                } else {
                    reference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            //checking if the user exists
                            if (dataSnapshot.child(rphone.getText().toString()).exists()) {
                                Snackbar.make(signupLayout, "User with the phone number already exists!", Snackbar.LENGTH_SHORT).show();
                            }
                            // if the user doesn't exist
                            else {
                                User user = new User(name.getText().toString(), rpswd.getText().toString());
                                reference.child(rphone.getText().toString()).setValue(user);
                                Snackbar.make(rootLayout, "User successfully registered!", Snackbar.LENGTH_SHORT).show();
                                signupDialog.dismiss();
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        closeImgOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupDialog.dismiss();
            }
        });
        signupDialog.show();
    }
}
