package com.example.mcdenny.crops;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.rengwuxian.materialedittext.MaterialEditText;

import mehdi.sakout.fancybuttons.FancyButton;

public class ForgotPassword extends AppCompatActivity {
    public FancyButton btnSubmit;
    public MaterialEditText txtPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setTitle("Forgot Password");


        btnSubmit = (FancyButton) findViewById(R.id.forgotBtn);
        txtPhone = (MaterialEditText) findViewById(R.id.txtForgotPhone);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgotPassword.this, ResetPassword.class);
                startActivity(intent);
            }
        });
    }
}
