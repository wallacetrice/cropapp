package com.example.mcdenny.crops;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.mcdenny.crops.interfaces.ItemClickListener;
import com.example.mcdenny.crops.model.Crop;
import com.example.mcdenny.crops.viewholder.CropViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class InfoActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirebaseDatabase database;
    DatabaseReference cropReference;
    FirebaseRecyclerAdapter<Crop, CropViewHolder> adapter;
    Crop newCrop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        setTitle("Crop List");

        //init firebase
        database = FirebaseDatabase.getInstance();
        cropReference = database.getReference("Crops");
        //load to the recycleview
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(false);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //loading the crops to the recycle view
        loadCrops();
    }

    //loading crops from firebase
    private void loadCrops() {
        adapter = new FirebaseRecyclerAdapter<Crop, CropViewHolder>(
                Crop.class,
                R.layout.crop_layout,
                CropViewHolder.class,
                cropReference
        ) {
            @Override
            protected void populateViewHolder(CropViewHolder viewHolder, Crop model, int position) {
                viewHolder.crpName.setText(model.getCrop());
                final String titleName = model.getCrop();

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClicked) {
                        Intent intent = new Intent(InfoActivity.this, CropDetail.class);
                        intent.putExtra("CropID", adapter.getRef(position).getKey());
                       // intent .putExtra("message_key", crpProfit);
                        Bundle bundle = new Bundle();
                        bundle.putString("Title_key", titleName);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }
        };
        adapter.notifyDataSetChanged();//Refresh data if it has been changed
        //Setting the adapter
        recyclerView.setAdapter(adapter);
    }
}
