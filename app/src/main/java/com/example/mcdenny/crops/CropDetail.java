package com.example.mcdenny.crops;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mcdenny.crops.model.Crop;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CropDetail extends AppCompatActivity {
    public TextView txtProf, txtSto, txtConditions, txtBest, txtDesc;
    public String txtTitle, prof, sto;
    FirebaseDatabase database;
    DatabaseReference cropDetail;
    Crop finalCrop;
    public String cropID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_detail);

        //init firebase
        database = FirebaseDatabase.getInstance();
        cropDetail = database.getReference("Crops");

        //reference to the views
        txtProf = (TextView) findViewById(R.id.variety);
        txtSto = (TextView) findViewById(R.id.storage);
        txtBest = (TextView) findViewById(R.id.bestAreas );
        txtConditions = (TextView) findViewById(R.id.conditions );
        txtDesc = (TextView) findViewById(R.id.desc );

        //Getting the crop list id from the crop list activity
        if (getIntent() != null){
            cropID  = getIntent().getStringExtra("CropID");
        }
        if(!cropID.isEmpty() && cropID != null){
            getCropDetail(cropID);
        }
    }

    //the crops details
    private void getCropDetail(final String cropID) {
        cropDetail .child(cropID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //populating the textviews from the database
                finalCrop = dataSnapshot.getValue(Crop.class);
                txtProf.setText(finalCrop.getCropVariety() );
                txtSto .setText(finalCrop.getStorage());
                txtBest.setText(finalCrop.getBestAreas());
                txtConditions.setText(finalCrop.getConditions());
                txtDesc.setText(finalCrop.getCropDescription());
                setTitle(finalCrop.getCrop());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
