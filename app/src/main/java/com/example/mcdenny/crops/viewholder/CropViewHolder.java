package com.example.mcdenny.crops.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.mcdenny.crops.R;
import com.example.mcdenny.crops.interfaces.ItemClickListener;

public class CropViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView crpName;
    private ItemClickListener itemClickListener;
    public CropViewHolder(@NonNull View itemView) {
        super(itemView);
        crpName =(TextView) itemView.findViewById(R.id.crop_name);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);
    }
}
