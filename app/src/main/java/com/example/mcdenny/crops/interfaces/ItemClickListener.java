package com.example.mcdenny.crops.interfaces;

import android.view.View;

/**
 * Created by McDenny on 9/6/2018.
 */

public interface ItemClickListener {
    //This will listen on the clicks on the items of the recyclerview
    void onClick(View view, int position, boolean isLongClicked);
}