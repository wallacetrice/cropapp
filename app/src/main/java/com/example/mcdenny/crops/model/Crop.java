package com.example.mcdenny.crops.model;

public class Crop {
    private String crop;
    private String cropDescription;
    private String conditions;
    private String bestAreas;
    private String cropVariety;
    private String storage;

    public Crop() {
    }

    public Crop(String crop, String cropDescription, String conditions, String bestAreas, String profitability, String storage) {
        this.crop = crop;
        this.cropDescription = cropDescription;
        this.conditions = conditions;
        this.bestAreas = bestAreas;
        this.cropVariety = profitability;
        this.storage = storage;
    }

    public String getCrop() {
        return crop;
    }

    public void setCrop(String crop) {
        this.crop = crop;
    }

    public String getCropDescription() {
        return cropDescription;
    }

    public void setCropDescription(String cropDescription) {
        this.cropDescription = cropDescription;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getBestAreas() {
        return bestAreas;
    }

    public void setBestAreas(String bestAreas) {
        this.bestAreas = bestAreas;
    }

    public String getCropVariety() {
        return cropVariety;
    }

    public void setCropVariety(String profitability) {
        this.cropVariety = profitability;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }
}
